<?php

namespace Laudis\Calculators\Models;

class PDOUserModel implements UserModel
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function listUsers() : array
    {
        $statement = $this->pdo->prepare("SELECT * FROM users");
        $statement->execute();
        $statement->bindColumn(1, $id, \PDO::PARAM_INT);
        $statement->bindColumn(2, $firstName, \PDO::PARAM_STR);
        $statement->bindColumn(3, $lastName, \PDO::PARAM_STR);

        $users = [];
        while ($statement->fetch(\PDO::FETCH_BOUND)) {
            $users[] = ['id' => $id, 'firstName' => $firstName, 'lastName' => $lastName];
        }
        return $users;
    }

    /**
     * add a user in the database
     * returns the user as array of data
     */

    public function addUser($firstName,$lastName) : array
    {
        $statement = $this->pdo->prepare("INSERT INTO users(firstname, lastname)  VALUES (?,?)");
        $statement->execute([$firstName,$lastName]);

        $user= ["firstName" => $firstName , "lastName" => $lastName];
        return $user;
    }

    /**
     * change a users firstname or lastname
     */
    // TODO reminder : if firstname or lastname are not filled in, take the old values. Id must be filled in
    public function updateUser($id, $newFirstName, $newLastName) : array
    {
        $getuser = $this->pdo->prepare("SELECT * from users WHERE id=?");
        $getuser->execute([$id]);
        $getuser->bindColumn(2,$oldFirstName,\PDO::PARAM_STR);
        $getuser->bindColumn(3,$oldLastName,\PDO::PARAM_STR);
        $getuser->fetch(\PDO::FETCH_BOUND);
        if ($newfirstName = null){
            $newFirstName = $oldFirstName;
        }
        if ($newlastName = null){
            $newLastName = $oldLastName;
        }
        $statement = $this->pdo->prepare("UPDATE users SET firstname=?,lastname=? WHERE id = ?");
        $statement->execute([$newFirstName,$newLastName,$id]);
        return ["updated" => $id, "oldFirstName"=> $oldFirstName, "newFirstName" => $newFirstName,"oldLastName" => $oldLastName, "newLastName" => $newLastName];
    }
    /**
     * delete a user with the userId
     */
    // TODO reminder : this doesnt return the correct values but deletes from the db
    //  need to runs this twice, first it deletes posts and second time it deletes user.
    public function deleteUser($id) : array
    {
            $statement = $this->pdo->prepare("DELETE FROM users WHERE id=?");
            $statement->execute([$id]);
            $userRows = $statement->rowCount();
            $statement = $this->pdo->prepare("DELETE FROM posts WHERE id=?");
            $statement->execute([$id]);
            $postRows = $statement->rowCount();

            $output = ["userRows" => $userRows,"postRows" => $postRows];
            return $output;
    }

    public function countAmountOfUsers(){
        $statement = $this->pdo->prepare("SELECT * FROM users");
        $statement->execute();

        return $statement->rowCount();
    }
}
