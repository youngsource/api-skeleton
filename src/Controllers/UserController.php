<?php

namespace Laudis\Calculators\Controllers;


use Laudis\Calculators\Contracts\ResponseWriterInterface;
use Laudis\Calculators\Models\UserModel;
use phpDocumentor\Reflection\Types\This;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;



/**
 * Class userController
 * @package controller
 * hier de parameters veraderen naar write to response (geef de data mee)
 */
class UserController extends BaseController
{
    private $userModel;

    public function __construct(ResponseWriterInterface $responseWriter, UserModel $userModel)
    {
        parent::__construct($responseWriter);
        $this->userModel = $userModel;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function listUsers(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $users = $this->userModel->listUsers();
        return $this->writeToResponse($response, ['users' => $users]);
    }

    public function deleteUser(ServerRequestInterface $request, ResponseInterface $response, array $params): ResponseInterface
    {
        $id = $request->getParsedBody()["id"];
        $users =  $this->userModel->deleteUser($id);
        return $this->writeToResponse($response,["deleted"=>$users]);
    }

    public function adduser(ServerRequestInterface $request, ResponseInterface $response, array $params): ResponseInterface{
        $firstName = $request->getParsedBody()["firstName"];
        $lastName = $request->getParsedBody()["lastName"];
        $user = $this->userModel->addUser($firstName,$lastName);

        return $this->writeToResponse($response,["added user" => $user]);
    }

    public function countAmountOfUsers(ServerRequestInterface $request, ResponseInterface $response, array $params): ResponseInterface{
        $amount = $this->userModel->countAmountOfUsers();
        return $this->writeToResponse($response,["amount" => $amount]);
    }

    public function updateUser(ServerRequestInterface $request, ResponseInterface $response, array $params): ResponseInterface{
        $id = $request->getParsedBody()["id"];
        $newFirstName= $request->getParsedBody()['newFirstName'];
        $newLastName = $request->getParsedBody()['newLastName'];

        $update = $this->userModel->updateUser($id,$newFirstName,$newLastName);

        return $this->writeToResponse($response,["updated" => $update]);
    }

}



